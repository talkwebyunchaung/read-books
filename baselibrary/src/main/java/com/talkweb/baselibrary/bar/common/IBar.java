package com.talkweb.baselibrary.bar.common;

/**
 * 单个条目的接口
 */
public interface IBar<D> extends IBarLayout.OnBarSelectedListener<D> {

    /**
     * 设置条目的数据
     *
     * @param data
     */
    void setBarInfo(D data);

    /**
     * 动态修改某个条目的大小
     *
     * @param height
     */
    void resetHeight(int height);
}
