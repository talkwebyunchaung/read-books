package com.talkweb.baselibrary.bar.component;

import com.talkweb.baselibrary.bar.bottom.BottomBarInfo;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionManager;

import java.util.List;

public class BottomBarComponentProvider extends BarComponentProvider {

    private List<BottomBarInfo<?>> mInfoList;

    public BottomBarComponentProvider(List<BottomBarInfo<?>> infoList, FractionManager fractionManager) {
        super(fractionManager);
        mInfoList = infoList;

    }

    @Override
    public Fraction getFraction(int position) {
        try {
            return mInfoList.get(position).fraction.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public int getCount() {
        return mInfoList.size();
    }

}
