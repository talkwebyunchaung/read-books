package com.talkweb.baselibrary.utils;

import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public final class ToastHelper {

    private ToastHelper() {
    }

    public static void showToastText(Context context, String message) {
        new ToastDialog(context).setText(message).show();
    }

    public static void showToastText(Context context, int message) {

        String messageS = ResourceHelper.getString(context, message);
        new ToastDialog(context).setText(messageS).show();
    }

    public static void showToastText(Context context, String message, boolean center) {
        new ToastDialog(context).setAlignment(center ? LayoutAlignment.CENTER : LayoutAlignment.BOTTOM).setText(message).show();
    }

    public static void showToastText(Context context, String message, int duration) {
        new ToastDialog(context).setText(message).setDuration(duration).show();
    }
}
