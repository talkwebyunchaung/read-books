package com.talkweb.baselibrary.base;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;

public abstract class BaseFractionAbilitySlice extends BaseAbilitySlice {

    public final FractionManager getFractionManager() {
        Ability ability = getAbility();
        if (ability instanceof FractionAbility) {
            FractionAbility fractionAbility = (FractionAbility) ability;
            return fractionAbility.getFractionManager();
        }
        return null;
    }

    protected final void addFraction(int layoutId, Fraction fraction) {
        FractionManager fractionManager = getFractionManager();
        if (fractionManager != null) {
            FractionScheduler fractionScheduler = fractionManager.startFractionScheduler();
            fractionScheduler.add(layoutId, fraction);
            fractionScheduler.submit();
        }
    }

    protected final void addFraction(int layoutId, Fraction fraction, String tag) {
        FractionManager fractionManager = getFractionManager();
        if (fractionManager != null) {
            FractionScheduler fractionScheduler = fractionManager.startFractionScheduler();
            fractionScheduler.add(layoutId, fraction, tag);
            fractionScheduler.submit();
        }
    }

    protected final void removeFraction(Fraction fraction) {
        FractionManager fractionManager = getFractionManager();
        if (fractionManager != null) {
            FractionScheduler fractionScheduler = fractionManager.startFractionScheduler();
            fractionScheduler.remove(fraction);
            fractionScheduler.submit();
        }
    }

    protected final void replaceFraction(int layoutId, Fraction fraction) {
        FractionManager fractionManager = getFractionManager();
        if (fractionManager != null) {
            FractionScheduler fractionScheduler = fractionManager.startFractionScheduler();
            fractionScheduler.replace(layoutId, fraction);
            fractionScheduler.submit();
        }
    }
}
