package com.talkweb.reader.circle;

import com.talkweb.baselibrary.base.BaseFraction;
import com.talkweb.baselibrary.utils.LogUtil;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.circle.adapter.CircleAdapter;
import com.talkweb.reader.circle.bean.ImageInfo;
import com.talkweb.reader.maintab.adapter.HomeAdapter;
import com.zhouyou.http.EasyHttp;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.ArrayList;
import java.util.List;


public class CircleFraction extends BaseFraction {
    private ListContainer listContainer;
    private CircleAdapter circleAdapter;
    private List<ImageInfo> listData=new ArrayList<>();
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

    }

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_fraction_circle;
    }

    @Override
    protected void initializeComponent(Component component) {
        listContainer = (ListContainer) component.findComponentById(ResourceTable.Id_circle_listContainer);
        initData();

    }

    private void initData() {
        circleAdapter = new CircleAdapter(getFractionAbility(),listData);
        listContainer.setItemProvider(circleAdapter);
        EasyHttp.get("app/getSquareInfs")
                .baseUrl("http://aimanpin.com")
                .params("page",10+"")
                .readTimeOut(30 * 1000)//局部定义读超时
                .writeTimeOut(30 * 1000)
                .connectTimeout(30 * 1000)
                .timeStamp(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                        LogUtil.e("data===",e.getMessage());

                    }

                    @Override
                    public void onSuccess(String response) {
                        if (response != null) {
                            LogUtil.e("data===",response);
                            parseData(response);
                        }
                    }
                });
    }

    private void parseData(String response) {
        listData.clear();
        List<ImageInfo> list= new ArrayList();
        for (int i = 0; i < 10; i++) {
            ImageInfo imageInfo=new ImageInfo();
            imageInfo.setBucketPositon("chasdaf");
            imageInfo.setId(i);
            imageInfo.setImageUrl("11111");
            list.add(imageInfo);
        }
        listData.addAll(list);
circleAdapter.notifyDataChanged();
    }

}
