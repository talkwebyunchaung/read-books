package com.talkweb.reader;

import com.zhouyou.http.EasyHttp;
import ohos.aafwk.ability.AbilityPackage;

public class MyApplication extends AbilityPackage {
    @Override
    public void onInitialize() {
        super.onInitialize();
        EasyHttp.init(this);
    }
}
