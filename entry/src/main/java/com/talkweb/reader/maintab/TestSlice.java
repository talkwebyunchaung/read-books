package com.talkweb.reader.maintab;

import com.talkweb.baselibrary.base.BaseAbilitySlice;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.component.BookPageComponent;

public class TestSlice extends BaseAbilitySlice {

    private BookPageComponent bookpage;


    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_test;
    }

    @Override
    protected void initComponent() {

        bookpage = findById(ResourceTable.Id_bookpage);


    }

    @Override
    protected void getData() {

    }


}
