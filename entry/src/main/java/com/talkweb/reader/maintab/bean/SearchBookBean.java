
package com.talkweb.reader.maintab.bean;


import com.talkweb.reader.bookshelf.bean.BookBean;

import java.io.Serializable;

/**
 * 获取图书详情页面
 */
public class SearchBookBean implements Serializable {
    /**
     * 图书url详情页地址
     */
    private String noteUrl;
    /**
     * 图书缩略图
     */
    private String coverUrl;

    /**
     * 图书名称
     */
    private String name;

    /**
     * 图书作者
     */
    private String author;

    private long words;

    private String state;
    /**
     * 最新章节
     */
    private String lastChapter;

    private Boolean isAdd = false;

    private String tag;

    private String kind;

    public String getUpdated() {
        return updated;
    }

    public void setUpdated(String updated) {
        this.updated = updated;
    }

    /**
     * 来源
     */
    private String origin;

    private String desc;
    /**
     * 更新时间
     */
    private String updated;


    public String getNoteUrl() {
        return noteUrl;
    }

    public void setNoteUrl(String noteUrl) {
        this.noteUrl = noteUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Boolean getAdd() {
        return isAdd;
    }

    public void setAdd(Boolean add) {
        isAdd = add;
    }

    public long getWords() {
        return words;
    }

    public void setWords(long words) {
        this.words = words;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLastChapter() {
        return lastChapter == null ? "" : lastChapter;
    }

    public void setLastChapter(String lastChapter) {
        this.lastChapter = lastChapter;
    }

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public BookBean toBookBean(){
        BookBean bookBean = new BookBean();
        bookBean.setAuthor(author);
        bookBean.setNoteUrl(noteUrl);
        bookBean.setCoverUrl(coverUrl);
        bookBean.setName(name);
        bookBean.setKind(kind);
        bookBean.setState(state);
        bookBean.setLastChapter(lastChapter);
        bookBean.setTag(tag);
        bookBean.setOrigin(origin);
        bookBean.setBookdesc(desc);
        bookBean.setUpdateTime(updated);
        return bookBean;
    }
}