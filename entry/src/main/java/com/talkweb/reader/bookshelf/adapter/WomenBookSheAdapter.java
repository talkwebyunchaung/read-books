package com.talkweb.reader.bookshelf.adapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.bean.BookBean;
import com.talkweb.reader.bookshelf.bean.KindBookShelfBean;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

public class WomenBookSheAdapter extends BaseItemProvider {

    private int mhotCount = 1;//最热数量
    private int mRecommendCount = 1;//推荐的数量
    private List<BookBean> hotRanking;
    private List<BookBean> recommend;
    private List<KindBookShelfBean> contentList;

    // 首先定义几个常量标记item的类型
    private static final int ITEM_TYPE_HOT = 0;
    private static final int ITEM_TYPE_CONTENT = 1;
    private static final int ITEM_TYPE_RECOMMEND = 2;

    private Context context;
    //中间内容位置信息
    private int mContentPosition;

    //各种点击时间接口,实现在fragment中
    private OnBookShelfAdapterClickListener listener;

    public void setNewData(List<BookBean> hotRanking,List<BookBean> recommend,List<KindBookShelfBean> contentList){
        this.hotRanking = hotRanking;
        this.recommend = recommend;
        this.contentList = contentList;
        if(this.hotRanking!=null){
            if(this.hotRanking.size()%2 == 0){
                this.mhotCount = this.hotRanking.size()/2;
            }else{
                this.mhotCount = this.hotRanking.size()/2 +1;
            }
        }
        notifyDataChanged();
    }

    public WomenBookSheAdapter(Context context, List<BookBean> hotRanking, List<BookBean> recommend, List<KindBookShelfBean> contentList ,
                               OnBookShelfAdapterClickListener listener) {
        this.context = context;
        this.listener = listener;
        this.hotRanking = hotRanking;
        this.recommend = recommend;
        this.contentList = contentList;
        if(this.hotRanking!=null){
            if(this.hotRanking.size()%2 == 0){
                this.mhotCount = this.hotRanking.size()/2;
            }else{
                this.mhotCount = this.hotRanking.size()/2 +1;
            }
        }
    }

    // 底部内容长度
    private int getContentItemCount() {
        return contentList.size();
    }

    // 判断当前item是否是最热头部（根据position来判断）
    private boolean isHotView(int position) {
        return mhotCount != 0 && position <= mhotCount && position!=0;
    }

    // 判断当前item是否为经典推荐位
    private boolean isRecommendView(int position) {
        return mRecommendCount != 0 && position ==0;
    }

    @Override
    public int getCount() {
        return contentList.size() + mhotCount + mRecommendCount;
    }

    @Override
    public Object getItem(int position) {
        return contentList.get(position - mhotCount - mRecommendCount);
    }

    @Override
    public long getItemId(int position) {
        return position - mhotCount - mRecommendCount;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        HotHolder headViewHolder = null;
        RecommendViewHolder recommendViewHolder = null;
        ListViewHolder listViewHolder = null;
        int type = getItemComponentType(position);
        mContentPosition = position - mhotCount - mRecommendCount;
        if (component == null) {
            switch (type) {
                case ITEM_TYPE_HOT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_hot, componentContainer, false);
                    headViewHolder = new HotHolder();
                    headViewHolder.titleLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_titleLayout);
                    headViewHolder.lay1 = (DependentLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    headViewHolder.lay2 = (DependentLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    headViewHolder.image1 = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_firstImage);
                    headViewHolder.image2 = (Image) component.findComponentById(ResourceTable.Id_mp_home_hot_twoImage);
                    headViewHolder.title1 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText);
                    headViewHolder.num1 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_fristText2);
                    headViewHolder.title2 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_twoText);
                    headViewHolder.num2 = (Text) component.findComponentById(ResourceTable.Id_mp_home_hot_twoText2);
                    component.setTag(headViewHolder);
                    break;
                case ITEM_TYPE_RECOMMEND:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_recommend, componentContainer, false);
                    recommendViewHolder = new RecommendViewHolder();
                    recommendViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    recommendViewHolder.twoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_twoLayout);
                    recommendViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    recommendViewHolder.mp_home_recommend_firstImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_twoImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_threeImage = (Image) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_fristText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_fristText);
                    recommendViewHolder.mp_home_recommend_twoText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_twoText);
                    recommendViewHolder.mp_home_recommend_threeText = (Text) component.findComponentById(
                            ResourceTable.Id_mp_home_recommend_threeText);
                    recommendViewHolder.mp_home_recommend_titleText = (Text) component.findComponentById(
                            ResourceTable.Id_titleLayout);
                    component.setTag(recommendViewHolder);
                    break;
                case ITEM_TYPE_CONTENT:
                    component = LayoutScatter.getInstance(context).parse(
                            ResourceTable.Layout_book_shelf_item_kind_list, componentContainer, false);
                    listViewHolder = new ListViewHolder();
                    listViewHolder.cardTitleImage = (Image) component.findComponentById(ResourceTable.Id_cardTitleImage);
                    listViewHolder.cardFristImage = (Image) component.findComponentById(ResourceTable.Id_cardFristImage);
                    listViewHolder.cardTowImage = (Image) component.findComponentById(ResourceTable.Id_cardTowImage);
                    listViewHolder.cardThreeImage = (Image) component.findComponentById(ResourceTable.Id_cardThreeImage);
                    listViewHolder.cardFourImage = (Image) component.findComponentById(ResourceTable.Id_cardFourImage);
                    listViewHolder.cardFiveImage = (Image) component.findComponentById(ResourceTable.Id_cardFiveImage);
                    listViewHolder.cardSixImage = (Image) component.findComponentById(ResourceTable.Id_cardSixImage);
                    listViewHolder.cardSevenImage = (Image) component.findComponentById(ResourceTable.Id_cardSevenImage);
                    listViewHolder.cardEightImage = (Image) component.findComponentById(ResourceTable.Id_cardEightImage);
                    listViewHolder.contentChange = (Image) component.findComponentById(ResourceTable.Id_contentChange);

                    listViewHolder.cardTitle = (Text) component.findComponentById(ResourceTable.Id_cardTitle);
                    listViewHolder.cardBookName = (Text) component.findComponentById(ResourceTable.Id_cardBookName);
                    listViewHolder.cardBookbref = (Text) component.findComponentById(ResourceTable.Id_cardBookbref);
                    listViewHolder.cardFristText = (Text) component.findComponentById(ResourceTable.Id_cardFristText);
                    listViewHolder.cardTowText = (Text) component.findComponentById(ResourceTable.Id_cardTowText);
                    listViewHolder.cardThreeText = (Text) component.findComponentById(ResourceTable.Id_cardThreeText);
                    listViewHolder.cardFourText = (Text) component.findComponentById(ResourceTable.Id_cardFourText);
                    listViewHolder.cardFiveText = (Text) component.findComponentById(ResourceTable.Id_cardFiveText);
                    listViewHolder.cardSixText = (Text) component.findComponentById(ResourceTable.Id_cardSixText);
                    listViewHolder.cardSevenText = (Text) component.findComponentById(ResourceTable.Id_cardSevenText);
                    listViewHolder.cardEightText = (Text) component.findComponentById(ResourceTable.Id_cardEightText);

                    listViewHolder.cardLayout = (StackLayout) component.findComponentById(ResourceTable.Id_cardLayout);
                    listViewHolder.firstLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_firstLayout);
                    listViewHolder.towLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_towLayout);
                    listViewHolder.threeLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_threeLayout);
                    listViewHolder.fourLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_fourLayout);
                    listViewHolder.fiveLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_fiveLayout);
                    listViewHolder.sixLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_sixLayout);
                    listViewHolder.sevenLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_sevenLayout);
                    listViewHolder.eightLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_eightLayout);
                    listViewHolder.mBookInfoLayout = (DirectionalLayout) component.findComponentById(ResourceTable.Id_mBookInfoLayout);

                    listViewHolder.rating = (Rating) component.findComponentById(ResourceTable.Id_rating);
                    component.setTag(listViewHolder);
                    break;
            }
        } else {
            switch (type) {
                case ITEM_TYPE_HOT:
                    headViewHolder = (HotHolder) component.getTag();
                    break;
                case ITEM_TYPE_RECOMMEND:
                    recommendViewHolder = (RecommendViewHolder) component.getTag();
                    break;
                case ITEM_TYPE_CONTENT:
                    listViewHolder = (ListViewHolder) component.getTag();
                    break;
            }
        }

        switch (type) {
            case ITEM_TYPE_HOT:
                headViewHolder.lay1.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component,hotRanking.get(2*(position-1)));
                    }
                });
                Glide.with(context)
                        .load(hotRanking.get(2*(position-1)).getCoverUrl())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(true)
                        .into(headViewHolder.image1);
                headViewHolder.title1.setText(hotRanking.get(2*(position-1)).getName());
                headViewHolder.num1.setText(hotRanking.get(2*(position-1)).getSearCount()+"万热度");
                if(hotRanking.size()>=position*2){
                    headViewHolder.lay2.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(hotRanking.get(2*(position-1)+1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(headViewHolder.image2);
                    headViewHolder.title2.setText(hotRanking.get(2*(position-1)+1).getName());
                    headViewHolder.num2.setText(hotRanking.get(2*(position-1)+1).getSearCount()+"万热度");
                    headViewHolder.lay2.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,hotRanking.get(2*(position-1)+1));
                        }
                    });
                }else{
                    headViewHolder.lay2.setVisibility(Component.INVISIBLE);

                }
                if(position == 1){
                    headViewHolder.titleLayout.setVisibility(Component.VISIBLE);
                }else{
                    headViewHolder.titleLayout.setVisibility(Component.HIDE);
                }
                break;
            case ITEM_TYPE_RECOMMEND:
                if(recommend.size() > 0){
                    Glide.with(context)
                            .load(recommend.get(0).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_firstImage);
                    recommendViewHolder.mp_home_recommend_fristText.setText(recommend.get(0).getName());
                    recommendViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(0));
                        }
                    });
                }
                if(recommend.size() > 1){
                    Glide.with(context)
                            .load(recommend.get(1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_twoImage);
                    recommendViewHolder.mp_home_recommend_twoText.setText(recommend.get(1).getName());
                    recommendViewHolder.twoLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(1));
                        }
                    });

                }
                if(recommend.size() > 2){
                    Glide.with(context)
                            .load(recommend.get(2).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(recommendViewHolder.mp_home_recommend_threeImage);
                    recommendViewHolder.mp_home_recommend_threeText.setText(recommend.get(2).getName());
                    recommendViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component,recommend.get(2));
                        }
                    });
                }
                recommendViewHolder.mp_home_recommend_titleText.setText("女生推荐");
                break;
            case ITEM_TYPE_CONTENT:
                KindBookShelfBean homeDesignBean = contentList.get(mContentPosition);
                List<BookBean> sourceContents = homeDesignBean.getSourceListContent();

                if(sourceContents.size() > 0){
                    Glide.with(context)
                            .load(sourceContents.get(0).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardTitleImage);
                }
                if(sourceContents.size() > 1) {
                    listViewHolder.firstLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(1).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardFristImage);
                    listViewHolder.cardFristText.setText(sourceContents.get(1).getName());
                    listViewHolder.firstLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(1));
                        }
                    });
                }else{
                    listViewHolder.firstLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 2) {
                    listViewHolder.towLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(2).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardTowImage);
                    listViewHolder.cardTowText.setText(sourceContents.get(2).getName());
                    listViewHolder.towLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(2));
                        }
                    });
                }else{
                    listViewHolder.towLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 3) {
                    listViewHolder.threeLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(3).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardThreeImage);
                    listViewHolder.cardThreeText.setText(sourceContents.get(3).getName());
                    listViewHolder.threeLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(3));
                        }
                    });
                }else{
                    listViewHolder.threeLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 4) {
                    listViewHolder.fourLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(4).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardFourImage);
                    listViewHolder.cardFourText.setText(sourceContents.get(4).getName());
                    listViewHolder.fourLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(4));
                        }
                    });
                }else{
                    listViewHolder.fourLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 5) {
                    listViewHolder.fiveLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(5).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardFiveImage);
                    listViewHolder.cardFiveText.setText(sourceContents.get(5).getName());
                    listViewHolder.fiveLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(5));
                        }
                    });
                }else{
                    listViewHolder.fiveLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 6) {
                    listViewHolder.sixLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(6).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardSixImage);
                    listViewHolder.cardSixText.setText(sourceContents.get(6).getName());
                    listViewHolder.sixLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(6));
                        }
                    });
                }else{
                    listViewHolder.sixLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 7) {
                    listViewHolder.sevenLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(7).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardSevenImage);
                    listViewHolder.cardSevenText.setText(sourceContents.get(7).getName());
                    listViewHolder.sevenLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(7));
                        }
                    });
                }else{
                    listViewHolder.sevenLayout.setVisibility(Component.HIDE);
                }
                if(sourceContents.size() > 8) {
                    listViewHolder.eightLayout.setVisibility(Component.VISIBLE);
                    Glide.with(context)
                            .load(sourceContents.get(8).getCoverUrl())
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .skipMemoryCache(true)
                            .into(listViewHolder.cardEightImage);
                    listViewHolder.cardEightText.setText(sourceContents.get(8).getName());
                    listViewHolder.eightLayout.setClickedListener(new Component.ClickedListener() {
                        @Override
                        public void onClick(Component component) {
                            listener.onItemClickListener(component, sourceContents.get(8));
                        }
                    });
                }else{
                    listViewHolder.eightLayout.setVisibility(Component.HIDE);
                }

                listViewHolder.contentChange.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onContentChangeClickListener(mContentPosition, homeDesignBean.getKind());
                    }
                });
                listViewHolder.cardTitle.setText(homeDesignBean.getKind());
                listViewHolder.cardBookName.setText(sourceContents.get(0).getName());
                listViewHolder.cardBookbref.setText(sourceContents.get(0).getBookdesc());
                ShapeElement shapeElementBg = new ShapeElement();
                shapeElementBg.setRgbColor(RgbColor.fromArgbInt(
                        Color.getIntColor(homeDesignBean.getCardColor())));
                listViewHolder.mBookInfoLayout.setBackground(shapeElementBg);

                listViewHolder.cardLayout.setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        listener.onItemClickListener(component, sourceContents.get(1));
                    }
                });
                break;
        }

        return component;
    }

    @Override
    public int getItemComponentType(int position) {
        if (isHotView(position)) {
            // 最热View
            return ITEM_TYPE_HOT;
        } else if (isRecommendView(position)) {
            return ITEM_TYPE_RECOMMEND;
        } else {
            // 内容View
            return ITEM_TYPE_CONTENT;
        }
    }

    public class HotHolder {
        DirectionalLayout titleLayout;
        DependentLayout lay1;
        DependentLayout lay2;
        Image image1;
        Image image2;
        Text title1;
        Text title2;
        Text num1;
        Text num2;
    }

    public class RecommendViewHolder {
        DirectionalLayout firstLayout;
        DirectionalLayout twoLayout;
        DirectionalLayout threeLayout;
        Image mp_home_recommend_firstImage;
        Image mp_home_recommend_twoImage;
        Image mp_home_recommend_threeImage;
        Text mp_home_recommend_fristText;
        Text mp_home_recommend_twoText;
        Text mp_home_recommend_threeText;
        Text mp_home_recommend_titleText;
    }

    public class ListViewHolder {
        Image cardTitleImage;
        Image cardFristImage;
        Image cardTowImage;
        Image cardThreeImage;
        Image cardFourImage;
        Image cardFiveImage;
        Image cardSixImage;
        Image cardSevenImage;
        Image cardEightImage;
        /**
         * 换一换图标
         */
        Image contentChange;

        Text cardTitle;
        Text cardBookName;
        Text cardBookbref;

        Text cardFristText;
        Text cardTowText;
        Text cardThreeText;
        Text cardFourText;
        Text cardFiveText;
        Text cardSixText;
        Text cardSevenText;
        Text cardEightText;

        StackLayout cardLayout;
        DirectionalLayout firstLayout;
        DirectionalLayout towLayout;
        DirectionalLayout threeLayout;
        DirectionalLayout fourLayout;
        DirectionalLayout fiveLayout;
        DirectionalLayout sixLayout;
        DirectionalLayout sevenLayout;
        DirectionalLayout eightLayout;
        DirectionalLayout mBookInfoLayout;
        /**
         * 首页Content布局中评分元素增加
         */
        Rating rating;
    }
}


