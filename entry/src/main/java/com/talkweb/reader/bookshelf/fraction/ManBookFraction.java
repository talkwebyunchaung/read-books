package com.talkweb.reader.bookshelf.fraction;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.talkweb.baselibrary.base.BaseFraction;
import com.talkweb.baselibrary.utils.LogUtil;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.adapter.BookSheAdapter;
import com.talkweb.reader.bookshelf.adapter.ManBookSheAdapter;
import com.talkweb.reader.bookshelf.adapter.OnBookShelfAdapterClickListener;
import com.talkweb.reader.bookshelf.bean.BookBean;
import com.talkweb.reader.bookshelf.bean.BookShelfBean;
import com.talkweb.reader.bookshelf.bean.KindBookShelfBean;
import com.talkweb.reader.bookshelf.bean.ManBookShelfBean;
import com.talkweb.reader.maintab.bean.SearchBookBean;
import com.talkweb.reader.maintab.detail.BookDetailAbility;
import com.zhouyou.http.EasyHttp;
import com.zhouyou.http.callback.SimpleCallBack;
import com.zhouyou.http.exception.ApiException;
import com.zhouyou.http.model.ApiResult;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;


public class ManBookFraction extends BaseFraction {

    private ListContainer listContainer;
    private ManBookSheAdapter bookAdapter;

    @Override
    protected int getUIContent() {
        return ResourceTable.Layout_fraction_book_recom;
    }

    @Override
    protected void initializeComponent(Component component) {
        listContainer = (ListContainer) component.findComponentById(
                ResourceTable.Id_listContainer);
        initData();
    }

    ManBookShelfBean bookDatas;
    private void initData() {
        EasyHttp.get("/appview/recommendManData")
                .baseUrl("http://aimanpin.com")
                .readTimeOut(30 * 1000)//局部定义读超时
                .writeTimeOut(30 * 1000)
                .connectTimeout(30 * 1000)
                .timeStamp(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(String response) {
                        if (response != null) {
                            Gson gson = new Gson();
                            ApiResult<ManBookShelfBean> bookShelfBeanApiResult = gson.fromJson(response, new TypeToken<ApiResult<ManBookShelfBean>>(){}.getType());
                            bookDatas = bookShelfBeanApiResult.getData();
                            LogUtil.e("BookData",bookShelfBeanApiResult.getData().toString());
                            initView();
                        }
                    }
                });
    }

    private void kindData(int position,String kinds) {
        EasyHttp.get("/appview/changByBookKind")
                .baseUrl("http://aimanpin.com")
                .readTimeOut(30 * 1000)//局部定义读超时
                .writeTimeOut(30 * 1000)
                .connectTimeout(30 * 1000)
                .params("kinds",kinds)
                .params("bookNumber","9")
                .timeStamp(true)
                .execute(new SimpleCallBack<String>() {
                    @Override
                    public void onError(ApiException e) {
                    }

                    @Override
                    public void onSuccess(String response) {
                        if (response != null) {
                            Gson gson = new Gson();
                            ApiResult<KindBookShelfBean> bookShelfBeanApiResult = gson.fromJson(response, new TypeToken<ApiResult<KindBookShelfBean>>(){}.getType());
                            KindBookShelfBean bookDatas = bookShelfBeanApiResult.getData();
                            if(bookDatas!=null){
                                ManBookFraction.this.bookDatas.getContentList().remove(position-1<=0?0:position-1);
                                ManBookFraction.this.bookDatas.getContentList().add(position-1<=0?0:position-1,bookDatas);

                                bookAdapter.setNewData(ManBookFraction.this.bookDatas.getHotRanking(),ManBookFraction.this.bookDatas.getRecommend(),ManBookFraction.this.bookDatas.getContentList());
                            }
                        }
                    }
                });
    }

    private void initView(){
        if(bookDatas == null){
            return;
        }
        bookAdapter = new ManBookSheAdapter(getFractionAbility(), bookDatas.getHotRanking(), bookDatas.getRecommend(), bookDatas.getContentList(), new OnBookShelfAdapterClickListener() {
            @Override
            public void onItemClickListener(Component component, BookBean bookBean) {
                SearchBookBean searchBookBean = new SearchBookBean();
                searchBookBean.setName(bookBean.getName());
                searchBookBean.setCoverUrl(bookBean.getCoverUrl());
                searchBookBean.setNoteUrl(bookBean.getNoteUrl());
                searchBookBean.setAuthor(bookBean.getAuthor());
                searchBookBean.setDesc(bookBean.getBookdesc());
                searchBookBean.setOrigin(bookBean.getOrigin());
                searchBookBean.setKind(bookBean.getKind());
                searchBookBean.setTag(bookBean.getTag());
                searchBookBean.setAdd(false);
                searchBookBean.setWords(0);

                Intent intent = new Intent();
                Operation operation = new Intent.OperationBuilder()
                        .withDeviceId("")
                        .withBundleName(getFractionAbility().getBundleName())
                        .withAbilityName(BookDetailAbility.class.getName())
                        .build();
                intent.setParam("searchBookBean", searchBookBean);
                intent.setOperation(operation);
                startAbility(intent);
            }

            @Override
            public void onContentChangeClickListener(int position, String kind) {
                kindData(position,kind);
            }
        });
        listContainer.setItemProvider(bookAdapter);
    }

}
