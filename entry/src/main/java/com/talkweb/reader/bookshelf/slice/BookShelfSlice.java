package com.talkweb.reader.bookshelf.slice;

import com.talkweb.baselibrary.bar.bottom.BottomBarInfo;
import com.talkweb.baselibrary.bar.bottom.BottomNavigationBar;
import com.talkweb.baselibrary.bar.component.BottomBarComponentProvider;
import com.talkweb.baselibrary.bar.component.FractionBarComponent;
import com.talkweb.baselibrary.bar.component.TopBarComponentProvider;
import com.talkweb.baselibrary.bar.top.TopBarInfo;
import com.talkweb.baselibrary.bar.top.TopNavigationBar;
import com.talkweb.baselibrary.base.BaseFractionAbilitySlice;
import com.talkweb.reader.ResourceTable;
import com.talkweb.reader.bookshelf.fraction.ManBookFraction;
import com.talkweb.reader.bookshelf.fraction.ReBookFraction;
import com.talkweb.reader.bookshelf.fraction.WomBookFraction;
import ohos.aafwk.ability.AbilityForm;
import ohos.aafwk.ability.OnClickListener;
import ohos.aafwk.ability.ViewsStatus;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;

import java.util.ArrayList;
import java.util.List;

public class BookShelfSlice extends BaseFractionAbilitySlice {

    /**
     * 底部导航栏的数据
     */
    private List<TopBarInfo<?>> bottomInfoList = new ArrayList<>();
    /**
     * 底部导航栏
     */
    private TopNavigationBar bottomNavigationBar;
    private FractionBarComponent mFractionBarComponent;
    private Image mBackImage;
    private int defaultColor;
    private int tintColor;


    @Override
    protected int getLayoutId() {
        return ResourceTable.Layout_ability_book_shelf_buy;
    }

    @Override
    protected void initComponent() {
        bottomNavigationBar = findById(ResourceTable.Id_bottomNavigationBar);
        mFractionBarComponent = findById(ResourceTable.Id_fractionbarComponent);
        mBackImage = findById(ResourceTable.Id_book_back_im);
        defaultColor = getColor(ResourceTable.Color_colorGray);
        tintColor = getColor(ResourceTable.Color_colorBlue);
        String recom = getString(ResourceTable.String_recom);
        String boy = getString(ResourceTable.String_boy);
        String gril = getString(ResourceTable.String_gril);
        // 推荐
        TopBarInfo<Integer> mianInfo = new TopBarInfo<>(recom,
                defaultColor, tintColor);
        mianInfo.fraction = ReBookFraction.class;
        // 男生
        TopBarInfo<Integer> bookInfo = new TopBarInfo<>(boy,
                defaultColor, tintColor);
        bookInfo.fraction = ManBookFraction.class;
        // 女生
        TopBarInfo<Integer> circleInfo = new TopBarInfo<>(gril,
                defaultColor, tintColor);
        circleInfo.fraction = WomBookFraction.class;

        bottomInfoList.add(mianInfo);
        bottomInfoList.add(bookInfo);
        bottomInfoList.add(circleInfo);

        // 设置底部导航栏的透明度
        // 初始化所有的条目
        bottomNavigationBar.initInfo(bottomInfoList);
        initFractionBarComponent();
        bottomNavigationBar.addBarSelectedChangeListener((index, prevInfo, nextInfo) ->
                // 显示fraction
                mFractionBarComponent.setCurrentItem(index));
        // 设置默认选中的条目，该方法一定要在最后调用
        bottomNavigationBar.defaultSelected(mianInfo);
        mBackImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminate();
                terminateAbility();
            }
        });
    }

    private void initFractionBarComponent() {
        FractionManager fractionManager = getFractionManager();
        TopBarComponentProvider provider = new TopBarComponentProvider(fractionManager,bottomInfoList);
        mFractionBarComponent.setProvider(provider);
    }

    @Override
    protected void getData() {

    }

    @Override
    protected void onBackPressed() {

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
